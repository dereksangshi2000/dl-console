import './command.dart';
import './errors.dart' show InvalidConfigMapError;

/// Console configuration.
abstract class ConsoleConfig {
  /// Create a console config by a map.
  factory ConsoleConfig.fromMap(Map configMap) => new ConsoleMapConfig(configMap);

  /// Get all the commands in a map.
  Map<String, ConsoleCommand> get commands;

  /// Get the console output.
  ConsoleOutput get consoleOutput;
}

/// Console config based on a map.
class ConsoleMapConfig implements ConsoleConfig {
  Map _configMap;

  Map _resolvedMap;

  ConsoleMapConfig(Map this._configMap);

  /// Resolve the passed config map.
  _resolve() {
    _resolvedMap = {};
    _resolvedMap['commands'] = _configMap.containsKey('commands')
        ? _configMap['commands']
        : {};
    if (!_configMap.containsKey('consoleOutput') || !(_configMap['consoleOutput'] is ConsoleOutput)) {
      throw new InvalidConfigMapError('consoleOutput',
          message: 'Console config is required and has to be a type of ConsoleOutput.');
    }
    _resolvedMap['consoleOutput'] = _configMap['consoleOutput'];
  }

  Map get resolvedMap {
    if (_resolvedMap == null) {
      _resolve();
    }
    return _resolvedMap;
  }

  Map<String, ConsoleCommand> get commands {
    return resolvedMap['commands'];
  }

  ConsoleOutput get consoleOutput {
    return resolvedMap['consoleOutput'];
  }
}
