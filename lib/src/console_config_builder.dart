import './command.dart';
import './console_config.dart';

/// The default console config builder (in a 'setter' way).
class ConsoleConfigBuilder {
  Map<String, ConsoleCommand> _commands = {};

  ConsoleOutput _consoleOutput;

  addCommand(String name, ConsoleCommand command) {
    _commands[name] = command;
  }

  setConsoleOutput(ConsoleOutput consoleOutput) {
    _consoleOutput = consoleOutput;
  }

  /// Build the console config.
  ConsoleMapConfig build() {
    return new ConsoleConfig.fromMap({
      'commands': _commands,
      'consoleOutput': _consoleOutput});
  }
}