/// Annotation for a console command.
class Command {
  final String _name;

  const Command(String this._name);

  String get name {
    return _name;
  }
}