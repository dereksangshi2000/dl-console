import 'dart:mirrors';

import './annotations.dart' show Command;
import './command.dart';
import './console_config.dart';

/// Console config factory to create different kinds of console configurations.
/// @todo The best way to getting all the commands via metadata is always via static code. So then,
///     1) No dart:mirrors is needed in run-time.
///     2) No objects caching (serialization & deserialization) is needed.
class ConsoleConfigMetadataBuilder {
  /// Create the immutable console config from metadata.
  ConsoleMapConfig build() {
    Map<String, ConsoleCommand> commands = {};
    var mirrorSystem = currentMirrorSystem();
    mirrorSystem.libraries.forEach((uri, LibraryMirror) {
      // Go through every member in the library.
      LibraryMirror.declarations.forEach((name, member) {
        // Only pick classes.
        if (member is ClassMirror) {
          // Search for metadata 'Command';
          var commandMetadataMirrors = member.metadata.where((instanceMirror) {
            return instanceMirror.reflectee is Command;
          });
          if (commandMetadataMirrors.isNotEmpty) {
            Command commandMetadata = commandMetadataMirrors.first.reflectee;
            var commandInstance = member.newInstance(new Symbol(''), []).reflectee;
            if (commandInstance is ConsoleCommand) {
              commands[commandMetadata.name] = commandInstance;
            }
          }
        }
      });
    });
    return new ConsoleConfig.fromMap({
      'commands': commands,
      'consoleOutput': new ConsoleOutput()});
  }
}