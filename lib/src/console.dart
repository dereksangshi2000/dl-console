import 'dart:io';
import 'dart:async';

import 'package:args/args.dart';
import './command.dart';
import './console_config.dart' show ConsoleConfig;
import './errors.dart' show OptionMissedError;

/// A console must have a run method.
abstract class Console {
  /// Create an async console.
  factory Console.standard(ConsoleConfig config) => new StandardConsole(config);

  /// Run the console.
  run(List<String> arguments);
}

/// A index of all the command instances.
class StandardConsole implements Console {
  /// The application configuration.
  ConsoleConfig _config;

  /// Argument parser.
  ArgParser _consoleArgParser;

  bool _isConfigured = false;

  /// The console default constructor.
  StandardConsole(ConsoleConfig this._config) : this._consoleArgParser = new ArgParser();

  /// Internal method to configure all the commands.
  _configureCommands() {
    _consoleArgParser.addFlag('help', abbr: 'h',
        help: 'Available commands: \n ${_config.commands.keys.join("\n")}');
    _config.commands.forEach(_configureCommand);
  }

  /// Configure the given command.
  _configureCommand(name, ConsoleCommand command) {
    ArgParser commandParser = new ArgParser();
    commandParser.addFlag('help', abbr: 'h', help: 'Show help message.');
    _consoleArgParser.addCommand(name, commandParser);
    command.configure(commandParser);
    _isConfigured = true;
  }

  /// Internal method to run the command according to received arguments.
  _runCommand(ArgResults argResults) async {
    var commandResults = argResults.command;
    if (commandResults is ArgResults) {
      // If the command is not found in the config, print the error and exit.
      // This if shouldn't be reached if everything is configured correctly since the commandResults
      // will be null if the command is not found.
      if (!_config.commands.containsKey(commandResults.name)) {
        _config.consoleOutput.error('Invalid command.');
        _printHelpAndExit();
      }

      // Print the sub command help message.
      if (commandResults['help']) {
        _printCommandHelpAndExit(commandResults.name);
      }

      // Execute the command.
      try {
        ConsoleCommand currentCommand = _config.commands[commandResults.name];
        if (currentCommand is AsyncConsoleCommand) {
          return await currentCommand.execute(commandResults, _config.consoleOutput);
        } else {
          return currentCommand.execute(commandResults, _config.consoleOutput);
        }
      } on OptionMissedError catch (exception) {
        _config.consoleOutput.error(exception.toString());
        _printCommandHelpAndExit(commandResults.name);
      } catch (exception, traceStack) {
        _config.consoleOutput.error(exception);
        _config.consoleOutput.error(traceStack.toString());
        _printCommandHelpAndExit(commandResults.name);
      }
    } else {
      _config.consoleOutput.error('Invalid command.');
      _printHelpAndExit();
    }
  }

  /// Print the given command help message and exist.
  _printCommandHelpAndExit(String commandName) {
    _config.consoleOutput.info(_consoleArgParser.commands[commandName].usage);
    exit(0);
  }

  /// Print the console help info.
  _printHelp() {
    _config.consoleOutput.info(_consoleArgParser.usage);
  }

  /// Print the console help and exist.
  _printHelpAndExit() {
    _printHelp();
    exit(0);
  }

  /// Run the application.
  Future run(List<String> arguments) async {
    // Configure all the commands.
    if (!_isConfigured) {
      _configureCommands();
    }
    // Run the corresponding command according to the arguments passed.
    try {
      ArgResults argResults = _consoleArgParser.parse(arguments);
      if (argResults['help']) {
        _printHelpAndExit();
      }
      return await _runCommand(argResults);
    } on OptionMissedError catch (exception) {
      _config.consoleOutput.error(exception.toString());
      _printHelp();
    } on FormatException catch (exception) {
      _config.consoleOutput.error(exception.toString());
      _printHelp();
    }
  }
}