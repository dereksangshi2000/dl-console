import 'package:dl_console/src/command.dart' show ConsoleOutput;
import 'package:dl_console/src/console_config_builder.dart' show ConsoleConfigBuilder;
import './console_app.dart' show ConsoleApp;

// Here all the commands.
import './command/say_hello_command.dart' show SayHelloCommand;
import './command/say_bye_command.dart' show SayByeCommand;

class Bootstrap {
  // Initialize the console app and return it.
  ConsoleApp init() {
    ConsoleConfigBuilder builder = new ConsoleConfigBuilder();
    builder
      ..setConsoleOutput(new ConsoleOutput())
      ..addCommand('say_hello', new SayHelloCommand())
      ..addCommand('say_bye', new SayByeCommand());
    return new ConsoleApp(builder.build());
  }
}