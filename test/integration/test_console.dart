import 'package:test/test.dart';

import '../mock/bootstrap.dart' show Bootstrap;
import '../mock/console_app.dart' show ConsoleApp;

run() async {
  test('test console app integrations.', () async {
    Bootstrap bootstrap = new Bootstrap();
    ConsoleApp app = bootstrap.init();
    expect(await app.run(['say_hello']), equals('Hello _anomynous_.'));
    expect(await app.run(['say_hello', '-n', 'derek']), equals('Hello derek.'));
    expect(await app.run(['say_bye', '-n', 'derek']), equals('Bye derek.'));
  });
}