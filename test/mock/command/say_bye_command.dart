import 'package:args/args.dart';

import 'package:dl_console/src/annotations.dart' show Command;
import 'package:dl_console/src/command.dart' show ConsoleCommand, ConsoleOutput;

/// Distribute files locally.
@Command('say_bye')
class SayByeCommand extends ConsoleCommand {

  /// Configure the command.
  configure(ArgParser argParser) {
    argParser
      ..addOption('name', abbr: 'n', help: 'The name to say goodbye to.', defaultsTo: '_anomynous_');
  }

  /// Run the command.
  execute(ArgResults argResults, ConsoleOutput consoleOutput) {
    return 'Bye ${argResults['name']}.';
  }
}