
/// The error when a service is not defined (service id not found).
class InvalidConfigMapError extends Error {
  String _field;
  String _message;

  InvalidConfigMapError(String this._field, {String message}) : this._message = message;

  String toString() {
    if (_message == null) {
      return "Field [${_field}] is invalid.";
    } else {
      return "Field [${_field}] is invalid: ${_message}.";
    }
  }
}

/// The error when a required option is missed.
class OptionMissedError extends Error {

  String _optionName;

  OptionMissedError(String this._optionName);

  String toString() {
    return "Option [${_optionName}] is missed.";
  }
}