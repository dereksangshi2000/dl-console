# dl_console

### Description
A simple wrapper of dart 'args' package for building a command line console with easily plugged in commands.

### Limitations
* Anything that 'package:args/args.dart' does not support.

### Samples
See [mock](./test/mock)