import 'dart:async';

import 'package:dl_console/src/console.dart';
import 'package:dl_console/src/console_config.dart' show ConsoleConfig;

/// The application root.
class ConsoleApp {

  ConsoleConfig _consoleConfig;

  Console _console;

  ConsoleApp(ConsoleConfig this._consoleConfig);

  /// Get the application console.
  Console get console {
    if (_console == null) {
      _console = new Console.standard(_consoleConfig);
    }
    return _console;
  }

  /// Run the application.
  Future run(List<String> arguments) async {
    return await this.console.run(arguments);
  }
}