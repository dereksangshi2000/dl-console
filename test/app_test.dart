// Copyright (c) 2017, derek. All rights reserved. Use of this source code
// is governed by a BSD-style license that can be found in the LICENSE file.

import 'integration/test_console.dart' as test_console;

main() async {
  await test_console.run();
}