import 'dart:async';

import 'package:args/args.dart';

/// Console printer.
class ConsoleOutput {

  static const TYPE_INFO = 1;
  static const TYPE_ERROR = 2;
  static const TYPE_ALERT = 3;

  const ConsoleOutput();

  /// Print the message according to the type code.
  _print(String message, {int type: ConsoleOutput.TYPE_INFO}) {
    // @todo Color the message according to the type.
    print(message);
  }

  /// Print out the message as normal information.
  info(String message) {
    _print(message);
  }

  /// Print the message as an error.
  error(String message) {
    _print(message, type: ConsoleOutput.TYPE_ERROR);
  }

  /// Print the message as an alert.
  alert(String message) {
    _print(message, type: ConsoleOutput.TYPE_ALERT);
  }
}

/// Command interface
abstract class ConsoleCommand {

  /// Configure the command arguments.
  configure(ArgParser argParser);

  /// Run the command with the passed arguments.
  execute(ArgResults argResults, ConsoleOutput consoleOutput);
}

/// Async command interface
abstract class AsyncConsoleCommand extends ConsoleCommand {

  @override
  Future execute(ArgResults argResults, ConsoleOutput consoleOutput);
}